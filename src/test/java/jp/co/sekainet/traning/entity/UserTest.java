package jp.co.sekainet.traning.entity;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.Test;

public class UserTest {

    @Test
    public void Userインスタンス同士の比較が正しく行えること() {
        User user = new User();
        user.setId(1);
        user.setLoginId("y.kono");
        user.setPassword("password");
        user.setName("河野");

        User user2 = new User();
        user2.setId(1);
        user2.setLoginId("y.kono");
        user2.setPassword("password");
        user2.setName("河野");

        assertThat(user, equalTo(user));
        assertThat(user, equalTo(user2));
    }

    @Test
    public void シリアライズとデシリアライズが行えること() throws Exception {
        User user = new User();
        user.setId(1);
        user.setLoginId("y.kono");
        user.setPassword("password");
        user.setName("河野");

        // userをバイト配列へシリアライズ
        ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutStream = new ObjectOutputStream(byteOutStream);
        objectOutStream.writeObject(user);
        byte[] serializedUser = byteOutStream.toByteArray();
        System.out.println("serializedUser: " + new String(serializedUser));

        // バイト配列からUserオブジェクトへデシリアライズ
        ByteArrayInputStream byteInStream = new ByteArrayInputStream(serializedUser);
        ObjectInputStream objectInStream = new ObjectInputStream(byteInStream);
        User deserializedUser = (User)objectInStream.readObject();

        assertThat(user, equalTo(deserializedUser));
    }
}
