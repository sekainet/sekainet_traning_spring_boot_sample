package jp.co.sekainet.traning.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import jp.co.sekainet.traning.BootWebApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BootWebApplication.class)
@WebAppConfiguration // U
public class UserServiceTest {

    @Autowired UserService userService;

    @Test
    public void パスワードのハッシュ化が行えること() {
        String password1Hash = userService.passwordToHash("password1");
        String password2Hash = userService.passwordToHash("password2");

        System.out.println("password1 hash:" + password1Hash);
        System.out.println("password2 hash:" + password2Hash);

        assertThat(password1Hash, equalTo(userService.passwordToHash("password1")));
        assertThat(password1Hash, not(equalTo(password2Hash)));
        assertThat(password1Hash, not(equalTo("password1")));

        assertThat(password2Hash, equalTo(userService.passwordToHash("password2")));
        assertThat(password2Hash, not(equalTo(password1Hash)));
        assertThat(password2Hash, not(equalTo("password2")));
    }
}
