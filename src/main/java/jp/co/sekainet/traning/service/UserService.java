package jp.co.sekainet.traning.service;

import java.security.MessageDigest;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import jp.co.sekainet.traning.entity.User;
import jp.co.sekainet.traning.repository.UserRepository;
import lombok.Getter;
import lombok.SneakyThrows;

@Service("userService")
@Scope( value=WebApplicationContext.SCOPE_SESSION,
        proxyMode=ScopedProxyMode.TARGET_CLASS)
public class UserService {

    @Autowired private UserRepository userRepo;
    @Getter private User loginUser;

    public boolean login(String loginId, String password) {
        User user = userRepo.findByLoginId(loginId);
        if (user == null) return false;
        if (user.getPassword().equals(password)) {
            loginUser = user;
            return true;
        } else {
            return false;
        }
    }

    public void logout() {
        loginUser = null;
    }

    public User findOne(long id) {
        return userRepo.findOne(id);
    }

    public List<User> findAll() {
        return userRepo.findAll();
    }

    public void save(User user) {
        userRepo.save(user);
    }

    public void delete(Long id) {
        userRepo.delete(id);
    }

    public boolean hasUniqueLoginId(User user) {
        User sameLoginIdUser = userRepo.findByLoginId(user.getLoginId());
        if (sameLoginIdUser == null) return true; // 一致するログインIDのユーザーがいなければOK
        if (sameLoginIdUser.getId() == user.getId()) return true; // 自分自身ならOK
        return false; // それ以外はNG
    }

    /** {@link #passwordToHash(String)}でパスワードのハッシュ化に使用するソルト. */
    private static final String SALT = "UYnGCzbwmnfsVnQ4mQ8P";

    /**
     * passwordを永続化しても安全なハッシュ値に変換する.
     * このメソッドでは安全なハッシュ値を生成するためにソルト・ストレッチング処理を行う.
     * @param password ハッシュ化の対象となるパスワード.
     * @return passwordのハッシュ値(Base64で文字列化). 44文字固定長.
     */
    @SneakyThrows // NoSuchAlgorithmExceptionは発生しない例外なのでthrowsから省略
    public String passwordToHash(String password) {
        String saltPassword = password + SALT;
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        byte[] passwordHash = sha256.digest(saltPassword.getBytes());
        for (int i = 0; i < 1000; i++) {
            passwordHash = sha256.digest(passwordHash);
        }
        return Base64.getEncoder().encodeToString(passwordHash);
    }
}
