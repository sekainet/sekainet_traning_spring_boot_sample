ALTER TABLE user ADD (
    phonetic        VARCHAR(100),
    postal_code     VARCHAR(8),
    prefecture_id   INTEGER NOT NULL DEFAULT 0,
    city            VARCHAR(50),
    address         VARCHAR(50),
    telephone       VARCHAR(20),
    email           VARCHAR(256),
    note            VARCHAR(500),
    admin           BOOLEAN NOT NULL DEFAULT FALSE
);